//  create object
let trainor = {
	firstname: "Mackoy",
	lastname: "Eubanas",
	work: function(){
		console.log("To train aspirant web developer.")
	}
};
console.log(trainor);
console.log(trainor.work);

function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.magic= 40;
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.health -= 40;
		console.log(`${this.name}'s health is now ${target.health}`);
	}
	this.defense = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.magic += 20;
		console.log(`${this.name}'s health is now ${target.magic}`);

	}
};

let Barry = new Pokemon("Barry");
let Boldore = new Pokemon("Boldore");

Barry.attack(Boldore);
Boldore.defense(Barry);

